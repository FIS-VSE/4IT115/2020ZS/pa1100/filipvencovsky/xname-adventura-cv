package cz.vse.java.xname.xnameadventuracv.main;

public interface Subject {

    /**
     * registrace pozorovatele
     * pozorovatelům se posílají změny
     * @param observer
     */
    void register(Observer observer);

    /**
     * odebrání pozorovatele ze seznamu
     * @param observer
     */
    void unregister(Observer observer);

    /**
     * upozorní všechny registrované pozorovatele na změnu
     * není vhodná jako veřejná metoda do rozhraní
     */
    void notifyObservers();
}
