package cz.vse.java.xname.xnameadventuracv.main;

public interface Observer {

    /**
     * metoda, kterou volá předmět pzorování (subject) při změně
     */
    void update();
}
